import './style.css'

let div = document.createElement('div')
div.innerHTML = '1 + 2 = '
div.onclick = () => {
  let currentDiv = div
  import('./module').then(
    ({ sum }) => currentDiv.innerHTML = `1 + 2 = ${sum(1,3)}`
  )
}
div.className = 'hi'
document.body.appendChild(div)

div = document.createElement('div')
div.innerHTML = `this is ${process.env.NODE_ENV}`
document.body.appendChild(div)
