import sum from './module'
import image from './webpack.jpg'
import './style.css'

let child = document.createElement('div');
child.innerHTML = `1 + 2 = ${sum(1,3)}`
child.className = 'hi'

document.body.appendChild(child)

let imageElement = new Image()
imageElement.src = image
child.appendChild(imageElement)
