import sum from './module'
import smallImage from './webpack.jpg'
import bigImage from './big.jpg'
import './style.css'

let child = document.createElement('div');
child.innerHTML = `1 + 2 = ${sum(1,3)}`
child.className = 'hi'

document.body.appendChild(child)

let imageElement = new Image()
imageElement.src = smallImage
child.appendChild(imageElement)

imageElement = new Image()
imageElement.src = bigImage
child.appendChild(imageElement)
