![LOGO](./assets/webpack.jpg)

---

### What is webpack?

+++?image=assets/what-is-webpack.png&size=60% auto

+++?image=assets/ssr-spa.png&size=auto 90%

+++

### Important Concepts
  - Entry point
  - Output
  - Loaders
  - Plugins
  - Configuration

+++?image=assets/loadorder.png&size=auto 90%

Note: explain created bundle

---

### Basic setup
  - Initialization and installation
  - Configuration
  - Running with npm
  - ES2015
  - Watch mode
  - Adding it to a webpage

Note: npm init and installation, create main/module files, create the config, add `npm run build` script, show `bundle.js`, change to ES2015 modules, use babel in config, create `index.html` to open in browser

---

### Important Plugins
  - HtmlWebpackPlugin
  - CleanWebpackPlugin
  - WebpackDevServer

Note: Add plugins to config

---

### Loaders

+++

### CSS
  - css-loader and style-loader

Note: Explain difference between css and style loader

+++

### Images and other files
  - file-loader
  - image-webpack-loader and url-loader

+++

### Other Loaders
  - babel-loader and ts-loader
  - eslint-loader and jshint-loader
  - html-loader
  - handlebars-loader, ejs-loader and pug-loader
  - sass-loader, less-loader and postcss-loader
  - xml-loader and json-loader
  - csv-loader

---

### Other Plugins
- DefinePlugin
- UglifyJSPlugin

Note: DefinePlugin to specify node env, show `--optimize-minimize`

---

### Code splitting
  - Entry points
  - CommonsChunksPlugin
  - Moving CSS to a separate file (extract-text-webpack-plugin)

Note: Prevent duplication with common chunks

+++

### Lazy loading
  - Dynamic imports
  - bundle-loader

+++

### Caching
  - Chunk naming

---

###  Development vs Production
  - webpack-merge

---

### Integrations
  - Grunt (grunt-webpack)
  - Gulp (webpack-stream)
  - Mocha (mocha-webpack)
  - Karma (karma-webpack)

---

### Links
  - [Webpack](https://webpack.js.org/)
  - [html-webpack-plugin](https://github.com/jantimon/html-webpack-plugin)
  - [clean-webpack-plugin](https://github.com/johnagan/clean-webpack-plugin)
  - [webpack-dev-server](https://github.com/webpack/webpack-dev-server)
  - [Loaders](https://webpack.js.org/loaders/)
  - [Plugins](https://webpack.js.org/plugins/)
  - [CommonsChunkPlugin](https://webpack.js.org/plugins/commons-chunk-plugin/)
  - [extract-text-webpack-plugin](https://github.com/webpack-contrib/extract-text-webpack-plugin)
  - [bundle-loader](https://github.com/webpack-contrib/bundle-loader)
  - [webpack-merge](https://github.com/survivejs/webpack-merge)
  - [Integrations](https://webpack.js.org/guides/integrations/)
