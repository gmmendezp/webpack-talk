import './style.css'
import load from 'bundle-loader?lazy!./module'

let div = document.createElement('div')
div.innerHTML = '1 + 2 = '
div.onclick = () => {
  load(
    ({ sum }) => div.innerHTML = `1 + 2 = ${sum(1,3)}`
  )
}
div.className = 'hi'
document.body.appendChild(div)
